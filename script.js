var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}

"use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gREQUEST_STATUS_OK = 200;
  const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
  const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

  var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
  var vArrayPopular = [];
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageLoading();
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    $.ajax({
      url: gBASE_URL + "/courses",
      type: "GET",
      dataType: 'json',
      success: function (res) {
        console.log(res);
        showDataForPopular(res);
        showDataForTrending(res)
      },
      error: function (error) {
        alert(error);
      }
    })
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm xử lý show dữ liệu cho Course Popular
  function showDataForPopular(paramPopular) {
    var newArray = paramPopular.filter(function (el) {
      return el.isPopular == true;
    });
    console.log(newArray)
    for (var i = 0; i < newArray.length; i++) {
      if (newArray[i].id == 8) {
        // course 1
        $('#img-popular-1').attr('src', newArray[i].coverImage);
        $("#title-popular-1").html(newArray[i].courseName);
        $("#p-popular-1").html("<i class='far fa-clock'></i>" + " " + newArray[i].duration + " " + newArray[i].level);
        $("#price-popular-1").html("$" + newArray[i].discountPrice + " " + "$" + "<del class='text-muted'>" + newArray[i].price + "</del>");
        $('#img-teacher-popular-1').attr('src', newArray[i].teacherPhoto);
        $("#name-popular-1").html(newArray[i].teacherName);
      }
      if (newArray[i].id == 6) {
        // course 2
        $('#img-popular-2').attr('src', newArray[i].coverImage);
        $("#title-popular-2").html(newArray[i].courseName);
        $("#p-popular-2").html("<i class='far fa-clock'></i>" + " " + newArray[i].duration + " " + newArray[i].level);
        $("#price-popular-2").html("$" + newArray[i].discountPrice + " " + "$" + "<del class='text-muted'>" + newArray[i].price + "</del>");
        $('#img-teacher-popular-2').attr('src', newArray[i].teacherPhoto);
        $("#name-popular-2").html(newArray[i].teacherName);
      }
      if (newArray[i].id == 9) {
        // course 3
        $('#img-popular-3').attr('src', newArray[i].coverImage);
        $("#title-popular-3").html(newArray[i].courseName);
        $("#p-popular-3").html("<i class='far fa-clock'></i>" + " " + newArray[i].duration + " " + newArray[i].level);
        $("#price-popular-3").html("$" + newArray[i].discountPrice + " " + "$" + "<del class='text-muted'>" + newArray[i].price + "</del>");
        $('#img-teacher-popular-3').attr('src', newArray[i].teacherPhoto);
        $("#name-popular-3").html(newArray[i].teacherName);
      }
      if (newArray[i].id == 5) {
        // course 4
        $('#img-popular-4').attr('src', newArray[i].coverImage);
        $("#title-popular-4").html(newArray[i].courseName);
        $("#p-popular-4").html("<i class='far fa-clock'></i>" + " " + newArray[i].duration + " " + newArray[i].level);
        $("#price-popular-4").html("$" + newArray[i].discountPrice + " " + "$" + "<del class='text-muted'>" + newArray[i].price + "</del>");
        $('#img-teacher-popular-4').attr('src', newArray[i].teacherPhoto);
        $("#name-popular-4").html(newArray[i].teacherName);
      }
    }
  }
  // Hàm xử lý show dữ liệu cho Course Popular
  function showDataForTrending(paramTrending) {
    var vArrTrending = paramTrending.filter(function (li) {
      return li.isTrending == true;
    });
    console.log(vArrTrending)
    for (var i = 0; i < vArrTrending.length; i++) {
      if (vArrTrending[i].id == 6) {
        // course 1
        $('#img-trending-1').attr('src', vArrTrending[i].coverImage);
        $("#title-trending-1").html(vArrTrending[i].courseName);
        $("#p-trending-1").html("<i class='far fa-clock'></i>" + " " + vArrTrending[i].duration + " " + vArrTrending[i].level);
        $("#price-trending-1").html("$" + vArrTrending[i].discountPrice + " " + "$" + "<del class='text-muted'>" + vArrTrending[i].price + "</del>");
        $('#img-teacher-trending-1').attr('src', vArrTrending[i].teacherPhoto);
        $("#name-trending-1").html(vArrTrending[i].teacherName);
      }
      if (vArrTrending[i].id == 1) {
        // course 2
        $('#img-trending-2').attr('src', vArrTrending[i].coverImage);
        $("#title-trending-2").html(vArrTrending[i].courseName);
        $("#p-trending-2").html("<i class='far fa-clock'></i>" + " " + vArrTrending[i].duration + " " + vArrTrending[i].level);
        $("#price-trending-2").html("$" + vArrTrending[i].discountPrice + " " + "$" + "<del class='text-muted'>" + vArrTrending[i].price + "</del>");
        $('#img-teacher-trending-2').attr('src', vArrTrending[i].teacherPhoto);
        $("#name-trending-2").html(vArrTrending[i].teacherName);
      }
      if (vArrTrending[i].id == 8) {
        // course 3
        $('#img-trending-3').attr('src', vArrTrending[i].coverImage);
        $("#title-trending-3").html(vArrTrending[i].courseName);
        $("#p-trending-3").html("<i class='far fa-clock'></i>" + " " + vArrTrending[i].duration + " " + vArrTrending[i].level);
        $("#price-trending-3").html("$" + vArrTrending[i].discountPrice + " " + "$" + "<del class='text-muted'>" + vArrTrending[i].price + "</del>");
        $('#img-teacher-trending-3').attr('src', vArrTrending[i].teacherPhoto);
        $("#name-trending-3").html(vArrTrending[i].teacherName);
      }
      if (vArrTrending[i].id == 11) {
        // course 4
        $('#img-trending-4').attr('src', vArrTrending[i].coverImage);
        $("#title-trending-4").html(vArrTrending[i].courseName);
        $("#p-trending-4").html("<i class='far fa-clock'></i>" + " " + vArrTrending[i].duration + " " + vArrTrending[i].level);
        $("#price-trending-4").html("$" + vArrTrending[i].discountPrice + " " + "$" + "<del class='text-muted'>" + vArrTrending[i].price + "</del>");
        $('#img-teacher-trending-4').attr('src', vArrTrending[i].teacherPhoto);
        $("#name-trending-4").html(vArrTrending[i].teacherName);
      }
    }
  }